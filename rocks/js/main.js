class MyHandler extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  afterPageLayout(pageFragment, page) {
    console.log(page);
    let lineheight = page.position * 1
    let fontwidth = 70 + page.position * 10
    page.area.style.lineHeight = 'calc(1em - '+lineheight+'px)'
    page.area.style.fontVariationSettings = '"wdth"'+ fontwidth
    page.area.style.display = "flex"
    page.area.style.flexDirection = "column-reverse"    
    page.pagebox.style.setProperty("--count", lineheight)
  }
}
Paged.registerHandlers(MyHandler);