(function rand(){
  var divsize = ((Math.random()*100) + 50).toFixed();
  $newdiv = $('<div/>');

  var posx = (Math.random() * 100);
  var posy = (Math.random() * 100);
  var deg = (Math.random() * 100);
  $newdiv.css({
      'position':'absolute',
      'left':posx+'vw',
      'top':posy+'vh',
      'transform':'rotate(' + deg + 'deg)',
  }).appendTo( '.fly' )
})();
