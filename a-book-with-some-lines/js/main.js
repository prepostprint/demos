
var nbrlines = 200;
window.PagedConfig = {
  before: () => {
    for(var i = 0; i< nbrlines; i++){
      line(i, 1, .7, 10, 2);
    }
    var booktitle = document.getElementById("booktitle");
    booktitle.innerHTML = nbrlines+" lines";
    var lastpage = document.createElement("section");
    lastpage.setAttribute("class","last");
    lastpage.innerHTML = "A book by Raphaël Bastide, generated with paged.js in 2018.<img src='https://cdn.glitch.com/6e9b9720-8dd2-420f-a3b5-714cd4213d51%2Fblue.png?1543597642575'>";
    container.appendChild(lastpage);
  }
};
