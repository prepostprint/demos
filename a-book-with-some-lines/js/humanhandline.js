function line(lineLength, penSize, trembling, minPeakLength, changePoints) {
  var dots = [];
  var absDots = [];
  var changePointList = []
  var peakLength = 0;
  var change = Math.round(rand(0,1));

  // Fill the dots array
  for (var i = 0; i < lineLength; i++) {
    dots.push("0");
  }

  // Distribute directions randomly
  for (var d = 0; d <= lineLength; d++) {
    var dotDirrection = Math.round(rand(-1, 1));
    dots[d] = dotDirrection;
    absDots[d] = dotDirrection
  }

  // Generate an array for change points
  var changeDirrection = Math.round(rand(-1, 1));
  for (var i = 0; i < changePoints; i++) {
    var changePointsIndex = Math.round(rand(0, lineLength))
    changePointList.push(changePointsIndex)
  }

  dots.forEach(function(item, index) {
    var dotDirrection = item;
    // if not last dot
    if (index < dots.length - 1) {
      var nextDotDirrection = dots[index + 1];
      // Apply trembling or not, depending on the % chances given by the trembling param
      var toggleTrembling = Number(Math.random() < trembling);
      // Change will force the same direction of the last dot
      if (toggleTrembling && change) {
      //   console.log('trembling + change');
        if (dotDirrection < 0) { // Keep the same direction up
          var newNextDotDirrection = dotDirrection - Math.abs(nextDotDirrection);
        }else { // Keep the same direction down
          var newNextDotDirrection = dotDirrection + Math.abs(nextDotDirrection);
        }
      }else if (toggleTrembling) {
        var newNextDotDirrection = dotDirrection + nextDotDirrection;
      }else {
        var newNextDotDirrection = dotDirrection;
      }
      if (peakLength <= minPeakLength) {
        newNextDotDirrection = dotDirrection
        peakLength++;
      }else {
        peakLength = 0;
      }
      if (isInArray(index, changePointList)) {
        change = !change;
      }
      dots[index + 1] = newNextDotDirrection;
    }
  });

  // Create the blocks from the array
  var line = document.createElement("section");
  line.setAttribute("class","part");
  var container = document.getElementById('container');
  var text = document.createElement('div');
  container.appendChild(line);
  dots.forEach(function(item, index, array) {
    var dot = document.createElement("div");
    dot.setAttribute(
      "style", `
      width: 1px;
      height: ${penSize}px;
      left: ${index}px;
      top: ${item}px;
    `);
    dot.setAttribute("class", "dot");
    line.appendChild(dot);
  });
  if (text) {    
    text.value = absDots.join(", ");
  }
}

// Random function helper
function rand(min, max) {
  return Math.random() * (max - min) + min;
}
// Is in array helper
function isInArray(value, array) {
  return array.indexOf(value) > -1;
}
