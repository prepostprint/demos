# A book with some lines

Change the `nbrlines` param in `js/main.js`!

A server is required for paged.js to run. If python is installed, cd to this directory `cd path/to/this/project/` then run `python3 -m http.server` otherwise look at [npm sevre](https://www.npmjs.com/package/serve)

![preview](a-book-with-10-lines.png)

## Context

Made in 2018 at Ensad for paged.js × PrePostPrint event.

## License

[GNU general public license V3](https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3))
