
#!/usr/bin/env python

import scribus
import sys
import os
from random import randint
magnetude = int(scribus.valueDialog("Magnetude",'Enter a magnetude number between 1 and 10',"1"))
page = int(scribus.valueDialog('What page to shake?',"Enter page number, '0' for all","1"))
pagenum = scribus.pageCount()
content = []
if page == 0:
	page = 1
else:
	pagenum = page
	pass
while (page <= pagenum):
	scribus.gotoPage(page)
	d = scribus.getPageItems()
	for item in d:
		rand = randint(- magnetude * 5 , magnetude * 5)
		itemname = item[0]
		print itemname
		scribus.rotateObjectAbs(rand,itemname)
		scribus.moveObject(rand,rand,itemname)
	page += 1
