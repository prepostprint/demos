# PPP Holes

Random holes in text.

A server is required for paged.js to run. If python is installed, cd to this directory `cd path/to/this/project/` then run `python3 -m http.server` otherwise look at [npm sevre](https://www.npmjs.com/package/serve)

![preview](holes.png)

## License

[GNU general public license V3](https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3))
