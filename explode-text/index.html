<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Explode</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" type="text/css" href="css/pagedjs-interface.css" />
    <link rel="stylesheet" type="text/css" href="css/main.css" />
  </head>
  <body>


    <h1 style="text-align:center">Permacomputing</h1>

    <p>This is a collection of random thoughts regarding the application of
    permacultural ideas to the computer world.</p>
    
    <p>See also: <a href="http://viznut.fi/texts-en/permacomputing_update_2021.html">Permacomputing
    update 2021</a></p>
    
    <p>Some have tried to connect these worlds before (<a href="http://wiki.c2.com/?PermaCulture">WikiWikiWeb's Permaculture
    article</a>; <a href="https://en.wikipedia.org/wiki/Kent_Beck">Kent
    Beck</a>'s short-lived idea of <a href="https://www.softwarequotes.com/showquotes.aspx?id=559&amp;name=Kent%20Beck">Permaprogramming</a>),
    but these have mostly concentrated on enhancing software engineering
    practices with some ideas from gardening. I am more interested in the aspect
    of cultural and ecological permanence. That is, how to give computers a
    meaningful and sustainable place in a human civilization that has a
    meaningful and sustainable place in the planetary biosphere.</p>
    
    <h2>1. Problem</h2>
    
    <p>Over the last few hundred years of human civilization, there has been a
    dramatic increase in the consumption of artificially produced energy. In the
    overarching story, this is often equated with "progress".</p>
    
    <p>In the computer world, this phenomenon gets multiplied by itself:
    "progress" facilitates ever greater densities of data storage and digital
    logic, thus dramatically exploding the availability of computing resources.
    However, the abundance has also caused an equivalent explosion in
    wastefulness, which shows in things like mindblowingly ridiculous hardware
    requirements for even quite trivial tasks.</p>
    
    <p>At the same time, computers have been failing their <a href="https://en.wikipedia.org/wiki/Computer_Lib/Dream_Machines">utopian
    expectations</a>. Instead of amplifying the users' intelligence, they rather
    amplify their stupidity. Instead of making it possible to scale down the
    resource requirements of the material world, they have instead become a
    major part of the problem. Instead of making the world more comprehensible,
    they rather add to its incomprehensibility. And they often even manage to
    become slower despite becoming faster.</p>
    
    <p>In both computing and agriculture, a major issue is that problems are too
    often "solved" by increasing controllability and resource use. Permaculture
    takes another way, advocating methods that "let nature do the work" and thus
    minimize the dependence on artificial energy input. Localness and
    decentralization are also major themes in the thought.</p>
    
    <p>What makes permacultural philosophy particularly appealing (to me) is
    that it does not advocate "going back in time" despite advocating a dramatic
    decrease in use of artificial energy. Instead, it trusts in human ingenunity
    in finding clever hacks for turning problems into solutions, competition
    into co-operation, waste into resources. Very much the same kind of creative
    thinking I appreciate in computer hacking.</p>
    
    <p>The presence of intelligent life in an ecosystem can be justified by its
    strengthening effect. Ideally, humans could make ecosystems more flexible
    and more resilient because of their ability to take leaps that are difficult
    or impossible for "unintelligent" natural processes. The existence of
    computers in a human civilization can be justified by their ability to
    augment this potential.</p>
    
    <h2>2. Physical resources</h2>
    
    <h3>2.1. Energy</h3>
    
    <p>Permaculture emphasizes resource-sensitivity. Computers primarily use
    electricity, so to them resource-sensitivity primarily means 1) adapting to
    changes in energy conditions and 2) using the available energy wisely.
    Today's computers, even mobile ones, are surprisingly bad at this. This is
    partially due to their legacy as "calculation factories" that are constantly
    guaranteed all the resources they "need".</p>
    
    <p>In permacomputing, intense non-urgent computation (such as long machine
    learning batches) would take place only when a lot of surplus energy is
    being produced or there is a need for electricity-to-heat conversion. This
    requires that the computer is aware of the state of the surrounding energy
    system.</p>
    
    <p>At times of low energy, both hardware and software would prefer to scale
    down: background processes would freeze, user interfaces would become more
    rudimentary, clock frequencies would decrease, unneeded processors and
    memory banks would power off. At these times, people would prefer to do
    something else than interact with computers.</p>
    
    <p>It is often wise to store energy for later use. <a href="https://en.wikipedia.org/wiki/Flywheel_energy_storage">Flywheels</a>
    are a potential alternative to chemical batteries. They have similar <a href="https://en.wikipedia.org/wiki/Energy_density">energy densities</a>
    (MJ/kg) but require no rare-earth materials and last for decades or
    centuries instead of mere years.</p>
    
    <h3>2.2. Silicon</h3>
    
    <p>IC fabrication requires large amounts of energy, highly refined machinery
    and poisonous substances. Because of this sacrifice, the resulting
    microchips should be treasured like gems or rare exotic spices. Their active
    lifespans would be maximized, and they would never be reduced to their raw
    materials until they are thoroughly unusable.</p>
    
    <p>Instead of planned obsolescence, there should be planned longevity.</p>
    
    <p>Broken devices should be repaired. If the community needs a kind of device
    that does not exist, it should preferrably be built from existing components
    that have fallen out of use. Chips should be designed open and flexible, so
    that they can be reappropriated even for purposes they were never intended
    for.</p>
    
    <p>Complex chips should have enough redundancy and bypass mechanisms to keep
    them working even after some of their internals wear out. (In a multicore
    CPU, for instance, many partially functioning cores could combine into one
    fully functioning one.)</p>
    
    <p>Chips that work but whose practical use cannot be justified can find
    artistic and other psychologically meaningful use. They may also be stored
    away until they are needed again (especially if the fabrication quality and
    the storage conditions allow for decades or centuries of "shelf life").</p>
    
    <p>Use what is available. Even chips that do "evil" things are worth
    considering if there's a landfill full of them. Crack their DRM locks,
    reverse-engineer their black boxes, deconstruct their philosophies. It might
    even be possible to reappropriate something like Bitcoin-mining ASICs for
    something artistically interesting or even useful.</p>
    
    <p>Minimized on-chip feature size makes it possible to do more computation
    with less energy but it often also means increased fragility and shorter
    lifespans. Therefore, the densest chips should be primarily used for
    purposes where more computation actually yields more. (In entertainment use,
    for example, a large use of resources is nothing more than a decadent
    esthetic preference.)</p>
    
    <p><a href="https://en.wikipedia.org/wiki/Unconventional_computing">Alternatives
    to semiconductors</a> should be actively researched. <a href="https://www.researchgate.net/publication/328395242_Towards_fungal_computer">Living
    cells</a> might be able to replace microchips in some tasks sometime in the
    future.</p>
    
    <p>Once perfectly clean ways of producing microchip equivalents have been
    taken to use, the need for "junk fetishism" will probably diminish.</p>
    
    <h3>2.3. Miscellaneous</h3>
    
    <p>Whenever bright external light is available, displays should be able to
    use it instead of competing against it with their own backlight. (See: <a href="https://en.wikipedia.org/wiki/Transflective_liquid-crystal_display">Transflective
    LCD</a>)</p>
    
    <p>Personally-owned computers are primarily for those who dedicate
    themselves to the technology and thus spend considerable amounts of time
    with it. Most other people would be perfectly happy with shared hardware.
    Even if the culture and society embraced computers more than anything else,
    requiring everyone to own one would be an overkill.</p>
    
    <h2>3. Observation and interaction</h2>
    
    <p>The first item in many lists of permacultural principles is "Observe and
    interact." I interpret this as primarily referring to a bidirectional and
    co-operative relationship with natural systems: you should not expect your
    garden to be easily top-down controllable like an army unit but accept its
    quirkiness and adapt to it.</p><p>
    
    </p><h3>3.1. Observation</h3>
    
    <p>Observation is among the most important human skills computers can augment.
    Things that are difficult or impossible for humans to observe can be brought
    within human cognitive capacity by various computational processes. Gathered
    information can be visualized, slight changes and pattern deviances
    emphasized, slow processes sped up, forecasts calculated. In Bill Mollison's
    words, "Information is <em>the</em> critical potential resource. It becomes
    a resource only when obtained and acted upon."</p>
    
    <p>Computer systems should also make their own inner workings as observable as
    possible. If the computer produces visual output, it would use a fraction of
    its resources to visualize its own intro- and extrospection. A computer that
    communicates with radio waves, for example, would visualize its own view of
    the surrounding radio landscape.</p>
    
    <p>Current consumer-oriented computing systems often go to ridiculous
    lengths to actually prevent the user from knowing what is going on. Even
    error messages have become unfashionable; many websites and apps just
    pretend everything is fine even if it isn't. This kind of extreme
    unobservability is a major source of technological alienation among computer
    users.</p>
    
    <p>The visualizations intended for casual and passive observation would be
    pleasant and tranquil while making it easy to see the big picture and notice
    the small changes. Tapping into the inborn human tendency to observe the
    natural environment may be a good idea when designing visualizers. When the
    user wants to observe something more closely, however, there is no limit in
    how flashy, technical and "non-natural" the presentation can be, as long as
    the observer prefers it that way.</p>
    
    <h3>3.2. Yin and yang hacking</h3>
    
    <p>Traditional computer hacking is often very "yang". A total understanding and
    control of the target system is valued. Changing a system's behavior is
    often an end in itself. There are predefined goals the system is pushed
    towards. Optimization tends to focus on a single measurable parameter.
    Finding a system's absolute limits is more important than finding its
    individual strengths or essence.</p>
    
    <p>In contrast, "yin" hacking accepts the aspects that are beyond rational
    control and comprehension. Rationality gets supported by intuition. The
    relationship with the system is more bidirectional, emphasizing
    experimentation and observation. The "personality" that stems from
    system-specific peculiarities gets more attention than the measurable specs.
    It is also increasingly important to understand when to hack and when just
    to observe without hacking.</p>
    
    <p>The difference between yin and yang hacking is similar to the difference
    between permaculture and industrial agriculture. In the latter, a piece of
    nature (the field) is forced (via a huge energy investment) into an
    oversimplified state that is as predictable and controllable as possible.
    Permaculture, on the other hand, emphasizes a co-operative (observing and
    interacting) relationship with the natural system.</p>
    
    <p>Yang hacking is quite essential to computing. After all, computers are
    based on comprehensible and deterministic models that tiny pieces of nature
    are "forced" to follow. However, there are many kinds of systems where the
    yin way makes much more sense (e.g. the behavior of neural networks is often
    very difficult to analyze rationally).</p>
    
    <p>Even the simplest programmable systems have a "yin" aspect that <a href="https://en.wikipedia.org/wiki/Halting_problem">stems from the
    programmability itself</a>. Also, taking the yang type of optimization to
    the extreme (like in the sub-kilobyte demoscene categories), one often bumps
    into situations where the yin way is the only way forward.</p>
    
    <p>Intellectual laziness may sometimes result in computing that is too yin.
    An example would be trying to use a machine learning system to solve a
    problem before even considering it analytically.</p>
    
    <h3>3.2.1. Processes</h3>
    
    <p>There are many kinds of computational processes. Some produce a final
    definitive result, some improve their result gradually. Some yield results
    very quickly, some need more time.</p>
    
    <p>Computing world still tends to prefer classic, mainframe-style processes
    that are one-shot and finite. No improvement over previous results, just
    rerun the entire batch from scratch. Even when a process is naturalistic,
    slow, gradual and open-ended – as in many types of machine learning –
    computer people often force it into the mainframeishly control-freaky
    framework. Some more "yin-type" attitude would be definitely needed.</p>
    
    <h2>4. Progress</h2>
    
    <p>The fossil-industrial story of linear progress has made many people
    believe that the main driver for computer innovation would be the constant
    increase of computing capacity. I strongly disagree. I actually think it
    would be more accurate to state that some innovation has been possible
    despite the stunting effect of rapid hardware growth (although this is not a
    particularly accurate statement either).</p>
    
    <p>The space of technological possibilities is not a road or even a tree:
    new inventions do not require "going forward" or "branching on the top" but
    can often be made from even quite "primitive" elements. The search space
    could be better thought about as a multidimensional rhizomatic maze:
    undiscovered areas can be expected to be found anywhere, not merely at the
    "frontier". The ability to speed fast "forward" on a "highway of technology"
    tends to make people blind to the diversity of the rhizome: the same boring
    ideas get reinvented with ever higher specs, and genuinely new ideas get
    downplayed.</p>
    
    <p>The linear-progressivist idea of technological obsolescence may stem from
    authoritarian metaphors: there may only be one king at a time. This idea
    easily leads to an impoverished and monocultural view of technology where
    there is room for only a handful of ideas at a time.</p>
    
    <p>Instead of technological "progress" (that implies constant abandoning of
    the old), we should consider expanding the diversity and abundance of ideas.
    Different kinds of technology should be seen as supporting each other rather
    than competing against each other for domination.</p>
    
    <p>In nature, everything is interdependent, and these interdependencies tend
    to strengthen the whole. In technology, however, large dependency networks
    and "diversity of options" often make the system more fragile. Civilization
    should therefore try to find ways of making technological dependencies work
    more like those in nature, as well as ways of embracing technological
    diversity in fruitful ways.</p>
    
    <h2>5. Programming</h2>
    
    <p>Programmability is the core of computing and the essence of computer
    literacy. Therefore, users must not be deliberately distanced from it.
    Instead, computer systems and user cultures should make programming as
    relevant, useful and accessible as possible.</p>
    
    <p>Any community that uses computers would have the ability to create its
    own software. A local software would address local needs better than the
    generic "one size fits all" solutions would.</p>
    
    <p>Rather than huge complex "engines" that can be reconfigured for different
    requirements, there would be sets of building blocks that could be used to
    create programs that only have the features necessary to fill their given
    purposes.</p>
    
    <p>Most of today's software engineering practices and tools were invented
    for a "Moore's law world" where accumulation, genericity and productization
    are more important than simplicity and resource-sensitivity. New practices
    and tools will be needed for a future world that will no longer approve
    wasteful use of resources.</p>
    
    <p>Optimization/refactoring is vitally important and should take place on
    all levels of abstraction, by both human and AI codecrafters.</p>
    
    <p>Ideally, it would be possible to invent and apply esoteric tricks without
    endangering the clarity or correctness of the main code (by separating the
    problem definition from implementation details, for example). It might be
    wise to maintain databases for problem solutions, optimization/refactoring
    tricks and reduction rules and develop ways to (semi)automatically find and
    apply them.</p>
    
    <h2>6. Software</h2>
    
    <p>There are many kinds of software, and very few principles apply to all of
    them. Some programs are like handheld tools, some programs are like
    intelligent problem-solvers, some programs are like gears in an engine, and
    some programs are nothing like any of those.</p>
    
    <h3>6.1. Dumb programs</h3>
    
    <p>A program that is intended to be like a tool should be understandable,
    predictable and wieldy. It should be simple enough that a proficient user
    can produce an unambiguous and complete natural-language description of what
    it does (and how). Ideally, the actual executable program would not be
    larger than this description.</p>
    
    <p>The ideal wieldiness may be compared to that of a musical instrument. The
    user would develop a muscle-memory-level grasp of the program features,
    which would make the program work like an extension of the user's body
    (regardless of the type of input hardware). There would be very few
    obstacles between imagination and expression.</p>
    
    <p>The absolute number of features is not as important as the flexibility of
    combining them. Ideally, this flexibility would greatly exceed the
    intentions of the original author of the program.</p>
    
    <h3>6.2. Smart programs</h3>
    
    <p>In addition to what is commonly thought as artificial intelligence,
    smartness is also required in tasks such as video compression and software
    compilation. Anybody/anything intending to perform these tasks perfectly
    will need to know a large variety of tricks and techniques, some of which
    might be hard to discover or very specific to certain conditions.</p>
    
    <p>It is always a nice bonus if a smart program is comprehensible and/or
    uses <a href="https://arxiv.org/abs/1907.10597">minimal resources</a>, but
    these attributes are by no means a priority. The results are the most
    important.</p>
    
    <p>One way to justify the large resource consumption of a smart program is
    to estimate how much resources its smartness saves elsewhere. The largest
    savings could be expected in areas such as resource and ecosystem planning,
    so quite large artificial brains could be justified there. Brains whose task
    is to optimize/refactor large brains may also be large.</p>
    
    <p>When expanding a "dumb" tool-like program with smartness, it should never
    reduce the comprehensibility and wieldiness of the core tool. It should also
    be possible to switch off the smartness at any time.</p>
    
    <h3>6.2.1. Artificial intelligence</h3>
    
    <p>Artificial intellects should not be thought about as competing against
    humans in human-like terms. Their greatest value is that they are different
    from human minds and thus able to expand the intellectual diversity of the
    world. AIs may be able to come up with ideas, designs and solutions that are
    very difficult for human minds to conceive. They may also lessen the human
    burden in some intellectual tasks, especially the ones that are not
    particularly suitable for humans.
    
    </p><p>Since we are currently in the middle of a global environmental crisis
    that needs a rapid and complete redesign of the civilization, we should
    co-operate with AI technology as much as we can.</p>
    
    <p>AI may also be important as artificial otherness. In order to avoid a
    kind of "anthropological singularity" where all meaning is created by human
    minds, we should learn to embrace any non-human otherness we can find. Wild
    nature is the traditional source of otherness, and a contact with
    extraterrestrial lifeforms would provide another. Interactions with
    artificial intelligence would help humans enrich their relationships with
    otherness in general.</p>
    
    <h3>6.3. Automation</h3>
    
    <p>Permaculture wants to develop systems where nature does most of the work,
    and humans mostly do things like maintenance, design and building. A good
    place for computerized automation would therefore be somewhere between
    natural processes and human labor.</p>
    
    <p>Mere laziness does not justify automation: modern households are full of
    devices that save relatively little time but waste a lot of energy.
    Automation is at its best at continuous and repetitive tasks that require a
    lot of time and/or effort from humans but only a neglectable amount of
    resources from a programmable device.</p>
    
    <h3>6.4. Maintenance</h3>
    
    <p>Many programs require long-term maintenance due to changing requirements
    and environments. This is an area where gardening wisdom can be useful. A
    major difference is that a software program is much easier to (re)create
    from scratch than a garden.</p>
    
    <p>Most changes to a program tend to grow its size/complexity. This effect
    should be balanced with refactoring (that reduces the size/complexity). The
    need for refactoring is often disregarded in today's "Moorean" world where
    software bloat is justified by constant hardware upgrades. In an ideal
    world, however, the constant maintenance of a program would be more likely
    to make it smaller and faster than to bloat it up.</p>
    
    <p>Programs whose functionality does not change should not require
    maintenance other than preservation. In order to eliminate "platform rot"
    that would stop old software from working, there would be compatibility
    platforms that are unambiguously defined, completely static (frozen) and
    easy to emulate/virtualize.</p>
    
    <h2>7. Culture</h2>
    
    <h3>7.1. Relationship with technology</h3>
    
    <p>Any community that uses a technology should develop a <a href="https://en.wikipedia.org/wiki/Technology_and_the_Character_of_Contemporary_Life">deep
    relationship</a> to it. Instead of being framed for specific applications,
    the technology would be allowed to freely connect and grow roots to all
    kinds of areas of human and non-human life. Nothing is "just a tool" or
    "just a toy", nobody is "just a user".</p>
    
    <p>There would be local understanding of each aspect of the technology. Not
    merely the practical use, maintenance and production but the cultural,
    artistic, ecological, philosophical and historical aspects as well. Each
    local community would make the technology <a href="https://en.wikipedia.org/wiki/Appropriate_technology">locally
    relevant</a>.</p>
    
    <p>Each technology would have one or more "scenes" where the related skills and
    traditions are maintained and developed. Focal practices are practiced,
    cultural artifacts are created, enthusiasm is roused and channeled,
    inventions are made. The "scenes" would not replace formal institutions or
    utilitarian practices but would rather provide an undergrowth to support
    them.</p>
    
    <p>No technology should be framed for a specific demographic segment or a
    specific type of people. The "scenes" should embrace and actively extend the
    diversity of their participants.</p>
    
    <p>Theoretical and practical understanding are equally important and support
    one another. Even the deepest academic theorist would sometimes make their
    hands dirty in order to strengthen their theory, and even the most pragmatic
    tinkerer would deepen their practice with some theoretical wisdom.</p>
    
    <h3>7.2. Telecommunication</h3>
    
    <p>The easiest way to send a piece of information between two computers
    should always be the one that uses the least energy without taking too much
    time. The allowable time would depend on the context: in some cases, a
    second would be too much, while in some others, even several days would be
    fine. If the computers are within the same physical area, direct
    peer-to-peer links would be preferred.</p>
    
    <p>When there are multiple simultaneous recipients for the same data,
    broadcast protocols would be preferred. For high-bitrate transfers (e.g.
    streaming video), shared broadcasts would also be culturally encouraged: it
    is a better idea to join a common broadcast channel than request a separate
    serving of the file.</p>
    
    <p>General-purpose communication platforms would not have entertainment as a
    design priority. The exchange of messages and information would be slow and
    contemplative rather than fast and reactive. In public discussion,
    well-thought-out and fact-based views would be the most respected and
    visible ones.</p>
    
    <p>Communication networks may very well be global and the protocols
    standardized, but the individual sites (platforms, forums, interfaces,
    BBSes) would be primarily local. Global, proprietary social media services
    would not be desirable, as they enforce the same "one size fits all"
    monoculture everywhere.</p>
    
    <p>All the most commonly needed information resources would be available at
    short or moderate distances. A temporary loss of intercontinental network
    connection would not be something most users would even notice.</p>
    
    <p>It should be easy to save anything from the network into local files.
    "Streaming-only" or other DRM-locked media would not exist.</p>
    
    <p>People would be aware of where their data is physically located and prefer
    to have local copies of anything they consider important.</p>
    
    <p>Any computer should be usable without a network connection.</p>
    
    <h3>7.3. Audiovisual media</h3>
    
    <p>Many people prefer to consume their audiovisual media at resolutions and
    data rates that are as high as possible (thus consuming as much energy as
    possible). This is, of course, an extremely unsustainable preference.</p>
    
    <p>There are countless ways, most of them still undiscovered, to make low
    and moderate data complexities look good — sometimes good enough that
    increased resolution would no longer improve them. Even compression
    artifacts might look so pleasant that people would actually prefer to have
    them.</p>
    
    <p>For extreme realism, perfection, detail and sharpness, people would prefer
    to look at nature.</p>
    
    <h3>7.4. Commons</h3>
    
    <p>Societies should support the development of software, hardware and other
    technology in the same way as they support scientific research and
    education. The results of the public efforts would be in the public domain,
    freely available and freely modifiable. Black boxes, lock-ins, excessive
    productization and many other abominations would be marginalized.</p>
    
    <hr>
    
    Written by Ville-Matias "<a href="http://www.viznut.fi/">Viznut</a>" Heikkilä.<br>
    <b>2020-06-24</b>: initial release<br>
    <b>2020-06-30</b>: license added, cosmetic changes<br>
    <b>2021-08-12</b>: linked to 2021 update<br>
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
    <img alt="Creative Commons License" style="border-width:0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFgAAAAfCAMAAABUFvrSAAAADFBMVEUAAAD///99fX2+w7rj0zguAAAA4klEQVRIx7WWgQ6FIAhFYf3/P9cjMX0K3rvStVwDT3QTRFS2jB/22DBkE9fI/WuwV639GnOrEILN/R6jKW4DCEaWftVWsWVJyjWX3M9NHTcne7j3fQH+42Zk8NNuy8BNlhTXOi3AqiM5V2KhhYGfv9xPH4DdV+oUrmGk8K1z3XzKwN2uQMBtJIDIwHZjwXCCUFIwKU39PKYIUduNKptMglCFnklp7gQhihB56M3L5tujOqqG75uA+dEU9wyWH6V5wMjR89CM2KVgzMf0IYpYRHE5ZskVRizKKA1g2Yi3tIU7sCfXlRBdibyVCwAAAABJRU5ErkJggg=="></a><br>
    This work is licensed under a
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
    Creative Commons Attribution 4.0 International License</a>.
    
    
    
  </body>
  <script type="text/javascript" src="js/main.js"></script>
  <script type="text/javascript" src="js/paged.polyfill.js"></script>
  <script>
	class MyHandler extends Paged.Handler {
		constructor(chunker, polisher, caller) {
			super(chunker, polisher, caller);
		}

    afterPreview(pages){
      new Explosion();
    }
		afterPageLayout(pageFragment, page) {
		}
	}
	Paged.registerHandlers(MyHandler);
  </script>
</html>
