### PrePostPrint Demos

Demonstration of web-to-print projects.

- HTML to print with CSS print (no js library)
- HTML to print with the [paged.js](https://www.pagedjs.org/) library 
- Python scripts to do generative layouts with [Scribus](https://www.scribus.net/)

Enter each directory to read its own README.